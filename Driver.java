package netcode.java;

public class Driver {

	public static void main(String[] args) {
		//Let's test for knight with special case and warrior with wp = 0
		//Ground is a square number
		Utility.Ground = 49;
		Knight knight1 = new Knight(500,0);
		Warrior warrior1 = new Warrior(400,0);
		//When there is a special case, the wp doesn't affect actual combat score
		//Expected to print 1000
		System.out.println(knight1.getCombatScore());
		//Expected to print 40
		System.out.println(warrior1.getCombatScore());
		
		
		//Test for warrior with special case
		//Ground is a prime number
		Utility.Ground = 47;
		Knight knight2 = new Knight(500,0);
		Warrior warrior2 = new Warrior(400,0); 
		//Expected to print 50
		System.out.println(knight2.getCombatScore());
		//When there is a special case, the wp doesn't affect actual combat score
		//Expected to print 800
		System.out.println(warrior2.getCombatScore());
		
		//Test for knight when ground is 999
		Utility.Ground = 999;
		Knight knight3 = new Knight(500,0);
		Warrior warrior3 = new Warrior(400,1); 
		//When there is a special case, the wp doesn't affect actual combat score
		//Expected to print 1597 (F17) since 987 (F16) is less than 1000
		System.out.println(knight3.getCombatScore());
		//Expected to print 400
		System.out.println(warrior3.getCombatScore());
		
		//Test for normal case
		Utility.Ground = 50;
		Knight knight4 = new Knight(500,1);
		Warrior warrior4 = new Warrior(400,1);
		//Expected to print 500
		System.out.println(knight4.getCombatScore());
		//Expected to print 400
		System.out.println(warrior4.getCombatScore());
	}
}
