package netcode.java;

public class Warrior extends Fighter{
	
	public Warrior(int baseHp,int wp) {
		super(baseHp,wp);
	}
	
	public double getCombatScore() {
		if (Utility.isPrime(Utility.Ground)) {
			return this.getBaseHp()*2;
		}
		else {
			if(this.getWp() == 1) return this.getBaseHp();
			else return this.getBaseHp()/10;
		}
	}
}
