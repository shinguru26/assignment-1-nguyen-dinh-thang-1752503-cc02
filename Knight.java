package netcode.java;

public class Knight extends Fighter{
	
	public Knight(int baseHp,int wp) {
		super(baseHp,wp);
	}
	
	public int Fibonacci(int begin) {
		if (begin == 0) return 0;
		if (begin == 1 || begin == 2) return 1;
		else return Fibonacci(begin - 1) + Fibonacci(begin - 2);
	}
	
	public double getFibonacci(double max, int begin) {
		if (max < Fibonacci(begin)) return Fibonacci(begin);
		else return getFibonacci(max,++begin);
	}
	
	public double getCombatScore() {
		if (Utility.isSquare(Utility.Ground)) {
			return this.getBaseHp()*2;
		}
		else if (Utility.Ground == 999) {
			return getFibonacci(this.getBaseHp()*2,1);
		}
		else {
			if(this.getWp() == 1) return this.getBaseHp();
			else return this.getBaseHp()/10;
		}
	}
}
